package com.example.bodymassindex;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.DatabaseReference;

public class signup extends AppCompatActivity {
    private EditText mfullname,memail,mpassword,phonenumber;
    private Button adminregister;
    private ProgressDialog mLoadingBar;
    private FirebaseAuth mAuth;
    FirebaseDatabase rootNode;
    DatabaseReference reference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        mfullname = findViewById(R.id.adminname);
        phonenumber = findViewById(R.id.adminphone);
        memail = findViewById(R.id.adminemail);
        mpassword = findViewById(R.id.adminpassword);
        adminregister = findViewById(R.id.adminbutton);

        mAuth = FirebaseAuth.getInstance();
        mLoadingBar = new ProgressDialog(signup.this);
        adminregister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkCrededentials();
                rootNode = FirebaseDatabase.getInstance();
                reference = rootNode.getReference("Users");

                String email = memail.getText().toString();
                String name = mfullname.getText().toString();
                String password = mpassword.getText().toString();
                String phone = phonenumber.getText().toString();

                UserHelperClass helperClass = new UserHelperClass(name, email, password,phone);
                reference.child(phone).setValue(helperClass);

            }
        });
    }
    private void checkCrededentials() {
        String email = memail.getText().toString();
        String name = mfullname.getText().toString();
        String password = mpassword.getText().toString();
        String phone = phonenumber.getText().toString();

        if (email.isEmpty() || !email.contains("@")) {
            memail.setError( "email is not valid");
            memail.requestFocus();
        }
        else if (name.isEmpty()) {
            mfullname.setError( "Username is not valid");
            mfullname.requestFocus();
        }

        else if (password.isEmpty() || password.length() < 8) {
            mpassword.setError( "Password is not valid");
            mpassword.requestFocus();
        }
        else if (phone.isEmpty() || phone.length() < 8) {
            phonenumber.setError( "invalid number");
            phonenumber.requestFocus();
        }
        else{
            mLoadingBar.setTitle("Signing Up");
            mLoadingBar.setMessage("Please wait while Signing Up");
            mLoadingBar.setCanceledOnTouchOutside(false);
            mLoadingBar.show();
            mAuth.createUserWithEmailAndPassword(email,password).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                @Override
                public void onComplete(@NonNull Task<AuthResult> task) {
                    if(task.isSuccessful()){
                        mAuth.getCurrentUser().sendEmailVerification().addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if(task.isSuccessful()){
                                    Toast.makeText(signup.this, "Successful registration,Check your email to verify", Toast.LENGTH_SHORT).show();
                                    mLoadingBar.dismiss();
                                    Intent intent= new Intent(signup.this,MainActivity.class);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(intent);
                                }
                                else{
                                    Toast.makeText(signup.this,task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                                }
                            }
                        });
                    }

                    else{
                        Toast.makeText(signup.this,task.getException().toString(), Toast.LENGTH_SHORT).show();
                    }

                }
            });

        }

    }

}