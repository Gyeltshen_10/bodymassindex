package com.example.bodymassindex;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class MainActivity extends AppCompatActivity {
    private EditText memail;
    private TextView forgotpas;
    private EditText mpassword;
    private Button login;
    private TextView Signup;
    private ProgressDialog mLoadingBar;
    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mAuth = FirebaseAuth.getInstance();
        memail = findViewById(R.id.email);
        mpassword = findViewById(R.id.password);
        login = findViewById(R.id.loginbutton);
        forgotpas=findViewById(R.id.forgotpass);
        Signup = findViewById(R.id.signup);

        mLoadingBar = new ProgressDialog(this);
        Signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this,signup.class);
                startActivity(intent);
            }
        });

        login.setOnClickListener(v -> {checkCrededentials();});
        mAuth=FirebaseAuth.getInstance();
        mLoadingBar=new ProgressDialog(MainActivity.this);
        Signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this,signup.class));
            }
        });
        forgotpas.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this,forgotpassword.class);
                startActivity(intent);
            }
        });
    }
    private void checkCrededentials() {
        String email = memail.getText().toString();
        String password = mpassword.getText().toString();

        if (email.isEmpty() || !email.contains("@")) {
            memail.setError( "email is not valid");
            memail.requestFocus();
        }
        else if (password.isEmpty()) {
            mpassword.setError( "Password is not valid");
            mpassword.requestFocus();
        }
        else if(password.length()<8){
            mpassword.setError( "Provide 8 character password");
            mpassword.requestFocus();
        }

        mLoadingBar.setTitle("Login");
        mLoadingBar.setMessage("Waiting for login");
        mLoadingBar.setCanceledOnTouchOutside(false);
        mLoadingBar.show();
        mAuth.signInWithEmailAndPassword(email,password).addOnCompleteListener(MainActivity.this,new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if(task.isSuccessful()){
                    if(mAuth.getCurrentUser().isEmailVerified()){
                        mLoadingBar.dismiss();
                        Intent intent= new Intent(MainActivity.this,userdashbord.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                    }
                    else{
                        Toast.makeText(MainActivity.this, "Please Verify your email address", Toast.LENGTH_SHORT).show();
                    }
                }
                else{
                    Toast.makeText(MainActivity.this,task.getException().toString(), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}